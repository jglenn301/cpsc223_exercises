#include <string.h>
#include <stdio.h>

// gcc -o ExB -Wall -pedantic -std=c99 -g3 cpsc223_f2019_exercise_b.c

typedef struct
{
  double lat;
  double lon;
} location;

typedef struct
{
  location loc;
  char *name;
} city;

int main(int argc, char **argv)
{
  // 1
  
  location coords[] = {{-23.5, -46.5}, {6.5, 3.3}, {13.7, 100.8}};
  char *names[] = {"GRU", "LOS", "BKK"};

  const int n = sizeof(coords) / sizeof(location); // int n = 3; would suffice

  // 2
  
  city sites[n];
  for (int i = 0; i < n; i++)
    {
      sites[i].loc = coords[i];
      sites[i].name = names[i];
    }

  city *tour[n];
  tour[0] = &sites[0];
  tour[1] = &sites[2];
  tour[2] = &sites[1];

  // 3
  
  char ticket[3][4];
  double *northernmost = &tour[0]->loc.lat;
  for (int i = 0; i < n; i++)
    {
      strcpy(ticket[i], tour[i]->name);
      if (tour[i]->loc.lat > *northernmost)
	{
	  northernmost = &tour[i]->loc.lat;
	}
    }

  // output things to check our answers
  for (int i =0 ; i < n; i++)
    {
      printf("%s\n", ticket[i]);
    }

  printf("%f\n", *northernmost);
}
